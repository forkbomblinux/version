This is a utility to check the version of a package.
It  works on Debian, RHEL, and SuSE/OpenSuSE.
Not being an experienced shell scripter, I need to know:
-How can I fetch the version of a package with PackageKit?
-How can I check whether the system is using dpkg, rpm, alpm, nix, guix, emerge, or one of the other dozens of other package managers out there?
I also have other plans for features, such as:
*-d --dependencies: List names and versions of the package and dependencies